.. include:: /helpers/roles.rst

.. rst-class:: ancestry
.. _ancestry--Hobgoblin:

Хобгоблин (Hobgoblin)
=============================================================================================================

.. epigraph::
	
	*Выше и сильнее своих гоблинских сородичей, хобгоблины по силе и размеру не уступают людям, и обладают широкими плечами и длинными мощными руками.*

-----------------------------------------------------------------------------

.. rst-class:: sidebar-char-ancestry-class

.. sidebar:: hidden

	.. rubric:: Редкость

	Необычный


	.. rubric:: Очки здоровья

	8


	.. rubric:: Размер

	Средний


	.. rubric:: Скорость

	25 футов


	.. rubric:: Повышения характеристик

	Телосложение

	Интеллект

	Свободное

	.. rubric:: Недостаток характеристики

	Мудрость


	.. rubric:: Языки

	Всеобщий

	Гоблинский

	Дополнительные языки в количестве вашего модификатора Интеллекта (если положительный).
	Выберите из Драконий, Дварфийский, Гнолльский, Йотун, Полуросликов, Орочий, и тех к которым у вас есть доступ (распространенные в вашем регионе).


	.. rubric:: Признаки

	Гоблин

	Гуманоид


	.. rubric:: Ночное зрение

	Вы можете видеть в темноте и при тусклом свете, как если бы это был яркий свет, однако ваше зрение в темноте - черно-белое.



Репутация хобгоблинов в регионе "Внутреннего моря" была подорвана Опраком, народом хобгоблинов, недавно основанным в горах между Нидалом и Нирмафасом.
"Легион Железного клыка", штурмовавший Мольтун и Нирмафас всего несколько лет назад, принял обдуманное решение прекратить боевые действия, объявить Опрак своей новой родиной и искать мира со своими соседями.
Теперь, получив строгий приказ не вступать в конфликты с другими народами, хобгоблины Опрака начали осторожно исследовать Авистан в духе сотрудничества, а не завоевания.
Многие люди, особенно те, кто пострадал от ужасных жестокостей легиона Железного Клыка, опасаются, что это просто пауза в агрессии, пока Опрак набирает силу, чтобы сокрушить своих соперников.
Другие надеются, что эти храбрые солдаты могут оказаться союзниками против бесконечных орд Шепчущего Тирана.



.. rst-class:: h3
.. rubric:: Вы можете ...

* Искать наиболее эффективные и практичные решения любой проблемы
* Поощрять четкую субординацию среди любой группы, с которой вы путешествуете, следуя приказам, даже если вы не согласны с ними
* Искать подходящие союзы с другими существами, даже если вы не полностью понимаете их или не доверяете им


.. rst-class:: h3
.. rubric:: Другие возможно ...

* Считают вас опасным из-за вашей репутации и устрашающей внешности
* Считают, что вы ненавидите магию и другие мистические искусства
* Признают вашу невероятную выносливость, преданность делу и дисциплину



.. rst-class:: h3
.. rubric:: Физическое описание

Хобгоблины почти такого же роста, как люди, хотя у них, как правило, более короткие ноги и более длинные руки и туловища.
У них лысые, широкие головы и глаза-бусинки, а серая кожа становится иссиня стальной, когда они загорают.
Хобгоблины удивительно выносливы; они быстрее выздоравливают от болезней и способны работать в течение длительного периода времени без особых трудностей.

Хобгоблины быстро взрослеют, и большинство из них могут ходить, говорить и держать оружие к тому времени, когда им исполняется 1 год, достигая подросткового возраста между  8 и 12 годами, а взрослой жизни около 14 лет.
Хобгоблины обычно живут до 70 лет.



.. rst-class:: h3
.. rubric:: Общество

Хобгоблины строят свое общество по военной иерархии.
Даже гражданские группы, такие как фермерские коллективы или торговые дома, объединяются в полки, роты и дивизии.
Большинство хобгоблинов зациклены на социальном статусе, и многие постоянно стремятся улучшить свое социальное положение.
Наказания хобгоблинов - это в первую очередь социальное понижение статуса, однако казнь или рабство обычны для серьезных преступлений, таких как поджог или дезертирство.

Ветераны хобгоблинов занимают высокое место в своем обществе, обычно становясь лидерами или советниками.
Магия редко практикуется и часто высмеивается, так как большинство хобгоблинов не доверяют ей, больше веря в силу своих собственных мечей.
Их искусство, как правило, имеет военный уклон; многие хобгоблины считают волнующие марши и изготовление оружия единственными художественными начинаниями, которыми стоит заниматься, хотя силой навязанная эпоха мира Опрака начинает это менять.



.. rst-class:: h3
.. rubric:: Мировоззрение и религия

Хотя большинство хобгоблинов знакомы с хаосом войны, они обладают упорядоченным разумом и предпочитают жить в рамках установленных иерархий.
В то время как многие хобгоблины считают сентиментальность слабостью, те кто обладает мягким темпераментом недавно добились успеха в дипломатии и международных контактах.

Как результат - хобгоблины-авантюристы обычно принципиально нейтральны, и только те, кто отвергает или воспитывается вне своего милитаристского общества, хаотичны.
Вера имеет мало места в обществе хобгоблинов, поскольку многие считают ее непрактичной, хотя религиозные хобгоблины могут получить скупую долю признания благодаря своей полезной целительной магии.



.. rst-class:: h3
.. rubric:: Хобгоблины-авантюристы (Hobgoblin Adventurers)

Жесткая милитаристская иерархия общества хобгоблинов производит превосходных авантюристов, поскольку хобгоблины достигают зрелого возраста, будучи подготовленными к бою.
Благодаря своему воспитанию, многие хобгоблины имеют предысторию воина.
Так же распространенными являются разнорабочий, ученик боевых искусств, шахтер и разведчик.
Хобгоблины выросшие в дали от своего народа часто имеют предысторию преступника, отшельника или кочевника.

Хобгоблины в основном воины, рейнджеры или плуты, хотя хобгоблины-плуты подходят к своему классу с дисциплинированным мышлением.
Хобгоблины имеют наследственную неприязнь к эльфам и недоверие к арканной магии, которую они называют "эльфийской магией".
Хобгоблины, практикующие арканную магию, чрезвычайно редки и обычно избегаются другими хобгоблинами.
Особенно умные хобгоблины обычно становятся алхимиками.



.. rst-class:: h3
.. rubric:: Имена

Как и у гоблинов, имена хобгоблинов, как правило, просты, хотя хобгоблинские имена обычно звучат более гортанно и сильно.
В редких случаях хобгоблины меняют свои имена, оставляя его основу, но добавляя аспекты, обычно в ответ на чрезвычайную травму или событие, изменяющее жизнь.
Хобгоблины не имеют фамилий, считая их бессмысленными и самонадеянными; достоинства и недостатки индивидуума должны быть заработаны их действиями, а не ассоциацией с определенным родом.

.. rst-class:: h4
.. rubric:: Пример имен

Эйз, Друкнар, Гаргэм, Хаткрен, Имакра, Кралэн, Мазкол, Олзу, Рэзал, Сивкраг, Волмак, Зорнум





Наследия хобгоблинов (Hobgoblin Heritages)
-----------------------------------------------------------------------------------------------------------

Хобгоблины имеют широкий спектр физиологических различий, основанных главным образом на их семейном происхождении.
На 1-м уровне выберите одно из следующих наследий тэнгу.


.. _ancestry-heritage--Hobgoblin--Elfbane:

Хобгоблин эльфогуб (`Elfbane Hobgoblin <https://2e.aonprd.com/Heritages.aspx?ID=40>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Character Guide pg. 49

Хобгоблины были созданы давным-давно из ненадежных и плодовитых гоблинов, чтобы использоваться в качестве армии против эльфов.
Хотя эльфы в конечном счете освободили хобгоблинов от их рабства, некоторые хобгоблины сохраняют наследственное сопротивление магии, которую они называют "эльфийской магией".
Вы получаете реакцию "Противиться эльфийской магии".

 .. rst-class:: description
 
Противиться эльфийской магии (`Resist Elf Magic <https://2e.aonprd.com/Actions.aspx?ID=316>`_) |д-р|
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

**Триггер**: Вы совершаете спасбросок против магического эффекта, но еще не сделали бросок

----------

Твое наследственное сопротивление магии защищает вас.
Вы получаете бонус обстоятельства +1 к спровоцировавшему спасброску.
Если спровоцировавший эффект арканный, вы вместо этого получаете бонус обстоятельства +2.



.. _ancestry-heritage--Hobgoblin--Runtboss:

Босс-коротышка (`Runtboss Hobgoblin <https://2e.aonprd.com/Heritages.aspx?ID=41>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Character Guide pg. 49

Вы происходите из длинного рода хобгоблинов, которые командовали гоблинами.
Ты меньше других хобгоблинов, но гоблины все равно слушаются любых команд, которые ты выкрикиваешь.
Вы получаете способность навыка :ref:`feat--Group-Coercion`.
Если при броске проверки Запугивания, вы получаете успех на :ref:`skill--Intimidation--Coerce` гоблина (но не других существ с признаком "гоблин"), то вместо этого вы получаете крит.успех; если при броске получаете крит.провал, то он становится просто провалом.


.. _ancestry-heritage--Hobgoblin--Smokeworker:

Курильщик (`Smokeworker Hobgoblin <https://2e.aonprd.com/Heritages.aspx?ID=42>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Character Guide pg. 49

Ваша семья была алхимиками, инженерами и учеными в течение многих поколений, делая проекты, дающие дым и огонь на поле битвы.
Вы получаете сопротивление огню, равное половине вашего уровня (минимум 1).
Вы автоматически преуспеваете в чистых проверках, когда целитесь в скрытое существо, если это скрытие вызвано дымом.


.. _ancestry-heritage--Hobgoblin--Warmarch:

Марширующий (`Warmarch Hobgoblin <https://2e.aonprd.com/Heritages.aspx?ID=43>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Character Guide pg. 49

Вы происходите из рода бродячих наемников, постоянно находящихся в походе и добывающих пищу на тракте.
Если вы проваливаете, но не крит.проваливаете проверку на :ref:`downtime--Subsist` в дикой местности, вы все равно можете обеспечить себя скудную пищу.
Во время исследования, вы можете :ref:`expl-activity--Hustle` в два раза дольше, прежде чем вам придется остановиться.


.. _ancestry-heritage--Hobgoblin--Warrenbred:

Выросший в лабиринте (`Warrenbred Hobgoblin <https://2e.aonprd.com/Heritages.aspx?ID=44>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Character Guide pg. 49

Твои предки жили под землей.
Ваши уши больше, чем у других хобгоблинов, и чувствительны к эхо.
Когда вы под землей, то можете использовать действие :ref:`action--Seek`, чтобы почувствовать необнаруженных существа в пределах 30-футового взрыва, вместо 15-футового взрыва как обычно.
В дополнение, если при броске проверки Акробатики на :ref:`skill--Acrobatics--Squeeze` вы получаете успех, то вместо этого вы получаете критический успех.



Универсальные наследия (Versatile Heritages)
-----------------------------------------------------------------------------------------------------------

.. include:: /helpers/versatile_heretages.rst





.. rst-class:: ancestry-class-feats
.. _ancestry--Hobgoblin-Feats:

Способности родословной
-----------------------------------------------------------------------------------------------------------

На 1-м уровне вы получаете одну способность родословной, и получаете дополнительную каждые 4 уровня после этого (на 5-м, 9-м, 13-м и 17-м уровнях).
Как хобгоблин, вы выбираете из следующих способностей.


1-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Hobgoblin--Alchemical-Scholar:

Алхимический ученый (`Alchemical Scholar <https://2e.aonprd.com/Feats.aspx?ID=1024>`_) / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 50

----------

Вы легче изучаете формулы.
Когда вы берете эту способность, то получаете 4 обычных алхимических формулы 1-го уровня, и каждый раз, когда вы получаете уровень, вы получаете обычную алхимическую формулу этого уровня.
Вам все еще нужна способность навыка :ref:`feat--Alchemical-Crafting`, чтобы :ref:`Создавать (Craft) <skill--Crafting--Craft>` алхимические предметы.

**Особенность**: Вы можете выбрать эту способность только на 1-м уровне и не можете переизучить ее на другую или другую способность на эту.


.. _ancestry-feat--Hobgoblin--Hobgoblin-Lore:

Знания хобгоблинов (`Hobgoblin Lore <https://2e.aonprd.com/Feats.aspx?ID=1025>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 50

----------

Вы изучали традиционные упражнения хобгоблинов и полевые работы, все из которых имеют милитаристский уклон.
Вы становитесь обучены Атлетике и Ремесле.
Если вы автоматически станете обучены одному из этих навыков (например, из-за вашей предыстории или класса), то вместо этого становитесь обучены другому навыку на свой выбор.
Вы так же становитесь обучены Знаниям хобгоблинов.


.. _ancestry-feat--Hobgoblin--Weapon-Familiarity:

Знакомство с оружием хобгоблинов (`Hobgoblin Weapon Familiarity <https://2e.aonprd.com/Feats.aspx?ID=1026>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 50

----------

Вы обучены обращению с длинными луками, короткими луками, композитными длинными луками, композитными короткими луками, глефами, копьями и длинными копьями.
В дополнение, вы получаете доступ ко всему необычному оружию хобгоблинов.
С целью определения вашего мастерства обращения с оружием, воинское оружие хобгоблинов является простым оружием, а улучшенное оружие хобгоблинов является воинским.


.. _ancestry-feat--Hobgoblin--Leech-Clipper:

Ловец "пиявок" (`Leech-Clipper <https://2e.aonprd.com/Feats.aspx?ID=1027>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 50

----------

Вы обучены ловить дезертиров или как их называют "пиявок".
Если вы критически попадаете по врагу оружием из группы "цеп", то можете обернуть оружие вокруг ног цели и бросить его, из-за чего враг получит штраф обстоятельства -10 футов Скорости к своим Скоростями, пока они сами или их союзники не распутают оружие, что занимает суммарно 2 действия :ref:`action--Interact`.


.. _ancestry-feat--Hobgoblin--Remorseless-Lash:

Безжалостная плеть (`Remorseless Lash <https://2e.aonprd.com/Feats.aspx?ID=1028>`_) / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 50

----------

Вы искусны в избиении врага, когда его моральный дух уже сломлен.
Когда вы успешно совершаете :ref:`action--Strike` ближнего боя по напуганному врагу, то этот враг не может снизить свое состояние "напуган" ниже 1 до начала вашего следующего хода.


.. _ancestry-feat--Hobgoblin--Vigorous-Health:

Крепкое здоровье (`Vigorous Health <https://2e.aonprd.com/Feats.aspx?ID=1029>`_) / 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 50

----------

Ваше телосложение крепкое и удивительно хорошо переносит кровопотерю.
Всякий раз, когда вы получите состояние "истощен", вы можете сделать чистую проверку с КС 17.
В случае успеха, вы не получаете состояние "истощен".





5-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Hobgoblin--Agonizing-Rebuke:

Мучительное осуждение (`Agonizing Rebuke <https://2e.aonprd.com/Feats.aspx?ID=1030>`_) / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 50

----------

Когда вы терроризируете своих врагов, вы также причиняете им болезненные душевные страдания.
Когда вы успешно :ref:`Деморализуете (Demoralize) <skill--Intimidation--Demoralize>` врага, он получает 1d4 ментального урона в начале каждого своего хода, пока остается с состоянием "напуган" и продолжает участвовать в сражении с вами.
Если вы мастер Запугивания, то урон увеличивается до 2d4, а если вы легенда, урон увеличивается до 3d4.


.. _ancestry-feat--Hobgoblin--Expert-Drill-Sergeant:

Сержант-инструктор (`Expert Drill Sergeant <https://2e.aonprd.com/Feats.aspx?ID=1031>`_) / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Источник**: Lost Omens: Character Guide pg. 51

----------

Вы знаете, как получить максимум от своих союзников.
Во время исследования, когда вы ведете их, а союзники делают :ref:`expl-activity--Follow-The-Expert`, вы даете бонус обстоятельства +3, вместо +2, если вы эксперт в навыке, и бонус обстоятельства +4 если вы мастер.


.. _ancestry-feat--Hobgoblin--Formation-Training:

Строевая подготовка (`Formation Training <https://2e.aonprd.com/Feats.aspx?ID=1032>`_) / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Предварительные условия**: обучен всему воинскому оружию

**Источник**: Lost Omens: Character Guide pg. 51

----------

Вы знаете, как сражаться в строю со своими собратьями.
Когда вы рядом с хотя бы двумя хобгоблинами-союзниками, вы получаете бонус обстоятельства +1 к КБ и спасброскам.
Этот бонус увеличивается до +2 на спасброски Рефлекса против эффектов по области.


.. _ancestry-feat--Hobgoblin--Weapon-Discipline:

Хобгоблинская тренировка с оружием (`Hobgoblin Weapon Discipline <https://2e.aonprd.com/Feats.aspx?ID=1033>`_) / 5
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Предварительные условия**: :ref:`ancestry-feat--Hobgoblin--Weapon-Familiarity`

**Источник**: Lost Omens: Character Guide pg. 51

----------

Вы знаете, как эффективно использовать оружие, которое солдаты используют в ближнем бою.
Когда вы делаете критическое попадание, используя оружие из группы "древковое", "копье" или "меч", то вы применяете эффект критической специализации оружия.





9-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Hobgoblin--Pride-in-Arms:

Гордость за оружие (`Pride in Arms <https://2e.aonprd.com/Feats.aspx?ID=1034>`_) |д-р| / 9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Триггер**: Союзник в пределах 30 футов снизит ОЗ противника до 0

**Источник**: Lost Omens: Character Guide pg. 51

----------

С триумфальным криком вы вдохновляете союзников на битву.
Спровоцировавший союзник до конца своего следующего хода получаете временные ОЗ, равные его модификатору Телосложения.





13-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Hobgoblin--Formation-Master:

Мастер строевой (`Formation Master <https://2e.aonprd.com/Feats.aspx?ID=1035>`_) / 13
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Предварительные условия**: :ref:`ancestry-feat--Hobgoblin--Formation-Training`

**Источник**: Lost Omens: Character Guide pg. 51

----------

Вы можете собрать отряд даже из представителей тех родословных, которым не хватает военной дисциплины хобгоблинов, и вы можете распространить эти преимущества на своих союзников-хобгоблинов.
Когда вы рядом с хотя бы двумя гуманоидными союзниками, вы получаете преимущества :ref:`ancestry-feat--Hobgoblin--Formation-Training`, даже если они не хобгоблины.
Союзники-хобгоблины рядом с вами и хотя бы один другой союзник-хобгоблин так же получают бонусы от способности :ref:`ancestry-feat--Hobgoblin--Formation-Training`.


.. _ancestry-feat--Hobgoblin--Weapon-Expertise:

Эксперт оружия хобгоблинов (`Hobgoblin Weapon Expertise <https://2e.aonprd.com/Feats.aspx?ID=1036>`_) / 13
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- хобгоблин

**Предварительные условия**: :ref:`ancestry-feat--Hobgoblin--Weapon-Familiarity`

**Источник**: Lost Omens: Character Guide pg. 51

----------

Вы повышаете свою подготовку с боевым оружием.
Всякий раз, когда вы получаете особенность класса, которая дает вам экспертное или выше мастерство владения определенным оружием, вы так же получаете его для всего оружия, с которым вы обучены от :ref:`ancestry-feat--Hobgoblin--Weapon-Familiarity`





17-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Hobgoblin--Azaersi's Roads:

Дороги Азэрси (`Azaersi's Roads <https://2e.aonprd.com/Feats.aspx?ID=2160>`_) / 17
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :rare:`редкая`
- хобгоблин

**Источник**: Lost Omens: Legends pg. 28

----------

Азэрси дала вам ограниченный доступ к "Камню Дорог", настроив вас на мельчайший осколок *"Ониксового ключа"*.
Вы получаете 
Вы получаете :ref:`spell--p--Plane-Shift` как врожденное природное заклинание.
Вы можете колдовать его дважды в неделю.
Его можно использовать только чтобы перемещаться между "Земляным планом" и "Материальным планом".
Из-за вашей настройки на *Ониксовый ключ*, вы можете служить фокусом заклинания и вам не требуется камертон.





.. include:: /helpers/actions.rst