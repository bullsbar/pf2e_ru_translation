.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--m--Mage-Armor:

Магический доспех (`Mage Armor <http://2e.aonprd.com/Spells.aspx?ID=176>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- преграждение

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Продолжительность**: до следующего дневного приготовления

----------

Вы окружаете себя мерцающей магической энергией, получая бонус предмета +1 к КБ и максимальный модификатор Ловкости +5.
Нося *магический доспех*, для расчета КБ, вы используете свой уровень мастерства защиты без доспеха.

----------

**Усиление (4-й)**: Вы получаете бонус предмета +1 к спасброскам.

**Усиление (6-й)**: Бонус предмета к КБ увеличивается до +2, и вы получаете бонус предмета +1 к спасброскам.

**Усиление (8-й)**: Бонус предмета к КБ увеличивается до +2, и вы получаете бонус предмета +2 к спасброскам.

**Усиление (10-й)**: Бонус предмета к КБ увеличивается до +3, и вы получаете бонус предмета +3 к спасброскам.





.. include:: /helpers/actions.rst