.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Spiritual-Weapon:

Духовное оружие (`Spiritual Weapon <http://2e.aonprd.com/Spells.aspx?ID=306>`_) / Закл. 2
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- разрушение
- сила

**Обычай**: сакральный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 120 футов

**Продолжительность**: поддерживаемое до 1 минуты

**Требования**: У вас есть божество

----------

Оружие, сделанное из чистой магической силы, материализуется и атакует врагов, которых вы указываете в пределах досягаемости.
Это оружие имеет призрачный вид дубинки, кинжала или выглядит как любимое оружие вашего божества.

Когда вы используете заклинание, оружие появляется рядом с врагом, которого вы выбираете в пределах досягаемости, и наносит по нему :ref:`action--Strike`.
Каждый раз, когда вы используете :ref:`action--Sustain-a-Spell`, то можете переместить оружие к новой цели (если необходимо) и совершить :ref:`action--Strike` по нему.
Духовное оружие использует и увеличивает ваш штраф множественной атак.

:ref:`Удары (Strike) <action--Strike>` оружием считаются атаками заклинанием ближнего боя.
Независимо от его внешнего вида, оружие наносит урон силой, равный 1d8 плюс ваш модификатор модификатор характеристики колдовства.
Вы можете наносить оружием обычный урон, вместо урона силой (или любой из доступного урона для "универсального" оружия).
Никакие другие характеристики и признаки оружия неприменимы, и даже дистанционное оружие атакует только существ рядом.
Несмотря на атаку заклинанием, духовное оружие считается оружием, для триггеров, сопротивлений и так далее.

Оружие не занимает пространство, не позволяет брать в тиски, и не имеет подобных характеристик присущих существу.
Оружие не может совершать других атак, кроме простого :ref:`action--Strike`, и способности или заклинания которые влияют на оружие, не могут быть применимы к нему.

----------

**Усиление (+2)**: Урон оружия увеличиваются на 1d8.

.. versionchanged:: /errata-r2
	Убран признак "атака".
	Оно имеет вид дубинки, кинжала или любимого оружия божества, для тех, кто может не иметь божества, как например чародеи.





.. include:: /helpers/actions.rst