.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--s--Summon-Summon-Instrument:

Призвать муз.инструмент (`Summon Instrument <https://2e.aonprd.com/Spells.aspx?ID=721>`_) / Чары 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- чары
- воплощение

**Обычай**: сакральный, оккультный

**Использование**: |д-3| жестовый, словесный, материальный

**Продолжительность**: 1 час

**Источник**: Advanced Player's Guide pg. 226

----------

Вы материализуете ручной музыкальный инструмент у себя в руках.
Инструмент является обычным для своего вида, но играет только для вас.
Он пропадает когда заклинание заканчивается.
Если вы снова колдуете *призвать инструмент*, то любой предыдущий призванный инструмент исчезает.

----------

**Усиление (5-й)**: Инструмент становится виртуозным ручным инструментом.





.. include:: /helpers/actions.rst