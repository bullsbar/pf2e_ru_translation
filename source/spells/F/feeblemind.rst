.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--f--Feeblemind:

Слабоумие (`Feeblemind <https://2e.aonprd.com/Spells.aspx?ID=88>`_) / Закл. 6
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- очарование
- недееспособность
- проклятие
- ментальное

**Обычай**: арканный, оккультный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 30 футов

**Цели**: 1 существо

**Спасбросок**: Воля

**Продолжительность**: различается

----------

Вы резко снижаете умственные способности цели.
Вы накладываете проклятие.
Цель должна пройти спасбросок Воли.

| **Критический успех**: Цель невредима.
| **Успех**: Цель "одурманена 2" на 1 раунд.
| **Провал**: Цель "одурманена 4" бессрочно.
| **Критический провал**: Интеллект цели навсегда становится ниже, чем у животного, и она считает свои модификаторы Харизмы, Интеллекта и Мудрости как -5. Теряет все классовые возможности, которые требуют умственных способностей, включая колдовство. Если цель - игровой персонаж, они становятся неигровыми персонажами под управлением Мастера.

.. versionadded:: /errata-r2
	Заострение внимания на том, что эффект накладывает проклятие.




.. include:: /helpers/actions.rst