.. include:: /helpers/roles.rst

.. rst-class:: spell
.. _spell--h--Hydraulic-Push:

Водный толчок (`Hydraulic Push <http://2e.aonprd.com/Spells.aspx?ID=154>`_) / Закл. 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- разрушение
- атака
- вода

**Обычай**: арканный, природный

**Использование**: |д-2| жестовый, словесный

**Дистанция**: 60 футов

**Цели**: 1 существо или ничейный объект

.. versionchanged:: /errata-r2
	Только ничейные объекты могут быть целью.

----------

Вы вызываете мощный удар воды под давлением, который бьет цель и отбрасывает ее назад.
Совершите дистанционную атаку заклинанием.

| **Критический успех**: Цель получает 6d6 дробящего урона и отталкивается назад на 10 футов.
| **Успех**: Цель получает 3d6 дробящего урона и отталкивается назад на 5 футов.

----------

**Усиление (+1)**: Урон увеличивается на 2d6.





.. include:: /helpers/actions.rst